all: report.tex
	xelatex -shell-escape report.tex
	xelatex -shell-escape report.tex

clean:
	rm -f *.{pdf,log,aux,out,toc,dvi}
